# Divel #
Divel, a rougelike rpg in golang, using sdl2 go bindings
# Team members #
## Artists ##
* strangerman123 (strangerman123#2869 on discord)
## Beta Testers ##
* [lordlom](https://github.com/lordlom) (lordlom#6911 on discord)
## Coders ##
* [haovi](https://github.com/haovipaws) (haovi#9721 on discord)
* [hamblingreen](https://github.com/hamblingreen)
